# Prerequisites

The user running the application needs to be enabled to write to Hawkular metrics:

```
oc create -f https://raw.githubusercontent.com/openshift/origin-metrics/master/metrics-cluster-role.yaml
oc adm policy add-cluster-role-to-user hawkular-metric-admin developer
```

And the user must authenticate via a Token: `oc whoami -t`

# Deployment

Review `app.py` and fill in the token... then deploy it