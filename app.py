#!/usr/bin/env python

"""app.py: This is a quick hack."""

from datetime import datetime
from hawkular.metrics import *
from prometheus_client import start_http_server, Counter
from time import sleep
from kafka import KafkaConsumer

import ssl, json

ctx = ssl.create_default_context()
ctx.check_hostname = False
ctx.verify_mode = ssl.CERT_NONE

c = Counter('deployments_tagged_total', 'Number of deployments tagged within Hawkular')
hawkular_client = HawkularMetricsClient(tenant_id='myproject',
                                        scheme='https',
                                        host='hawkular-metrics.openshift-infra.svc',
                                        port=443,
                                        cafile='/run/secrets/kubernetes.io/serviceaccount/ca.crt',
                                        context=ctx,
                                        token='Qcw0WulxPg9a_t16ip2Ug8kYyKLEo0y6abdwjTVJZu0')

t = datetime.utcnow()
datapoint = create_datapoint('deployment', t)
metric = create_metric(MetricType.String, 'events.deployment', [datapoint])
c.inc()     # Increment by 1

hawkular_client.put(metric)

kafka_consumer = KafkaConsumer('eventrouter', bootstrap_servers='kafka:9092')

if __name__ == '__main__':
    start_http_server(8080)

    for msg in kafka_consumer:
        payload = json.loads(str(msg.value,'utf-8'))
        print(payload['event']['reason'])